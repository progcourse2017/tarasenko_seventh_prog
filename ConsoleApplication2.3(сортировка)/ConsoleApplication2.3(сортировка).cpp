#include "stdafx.h"
#include <iostream>
using namespace std;
bool sort(int *arr, int size) 
{
	int value=0;
	for (int j = 0; j < size-1; j++)
	{
		for (int i = 0; i < size - j-1; i++)
		{
				if (arr[i] > arr[i + 1])
				{
					value = arr[i + 1];
					arr[i + 1] = arr[i];
					arr[i] = value;
				}
		}
	}
	return true;
}


int main()
{
	setlocale(LC_ALL, "russian");
	int size;
	cout << "Введите размер массива:" << endl;
	cin >> size;
	int *array=new int[size];
	cout << "Введите значения массива:" << endl;
	for (int i = 0; i < size; i++)
	{
		cin >> array[i];
	}
	bool quickSort = sort(array, size);
	if (quickSort == true)
	{
		for (int i = 0; i < size; i++)
		{
			cout << array[i] << " ";
		}
	}
	system("pause");
    return 0;
}

